class Vector2D:

    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def __str__(self):
        return "<%s, %s>" % (self.x, self.y)

    def __add__(self, v):
        return Vector2D(self.x + v.x, self.y + v.y)

    def __call__(self, *args, **kwargs):
        print("crly ? u r callin' vec?")

    def __mul__(self, k):
        if type(k) == int:
            return Vector2D(self.x * k + self.y * k)
        else:
            return Vector2D(self.x * k.x, self.y * k.y)

    def __sub__(self, k):
        return Vector2D(self.x - k.x, self.y - k.y)

    def __abs__(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5

    def __eq__(self, other):
        return True if self.x == other.x and self.y == other.y else False

v1 = Vector2D(1, 2)
v2 = Vector2D(1, 2)
v3 = v1 + v2

print(v2 - v1)
print(v3)
print(abs(v1))
print(v1 == v2)
