import datetime

f = open("timetable.txt")
teachers = []
items = []
subjects = []


class Item:
    def __init__(self, weekday, t, subj, teacher):
        self.weekday = weekday
        self.time = t
        self.subj = subj
        self.teacher = teacher


weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
weekday = ""

for line in f:
    lines = line.split("\t")
    if line[:3] in weekdays:
        weekday = line[:3]
        continue
    teacher = lines[2].replace("\n", "")
    items.append(Item(weekday, lines[0], lines[1], teacher))
    if not lines[1] in subjects:
        subjects.append(lines[1])
    if not teacher in teachers:
        teachers.append(teacher)

def print_time(stat_teachers, stat_subjects, count):
    print("\nОбъем работы каждого преподавателя\n")
    for k, v in stat_teachers.items():
        print("%s: %s" % (k, v))
    print("\nВремя учебы по каждому предмету\n")
    for k, v in stat_subjects.items():
        print("%s: %s" % (k, v))
    print("\nОбщее время учебы\n")
    print(count)

def calc_hours(items):
    calc_teachers = dict.fromkeys(teachers)
    calc_subjects = dict.fromkeys(subjects)
    count = datetime.timedelta()

    for i in range(len(items)):
        delta = datetime.timedelta()
        timeD = items[i].time.split("-")
        time1 = datetime.datetime.strptime(timeD[0], "%H:%M")
        time2 = datetime.datetime.strptime(timeD[1], "%H:%M") if len(timeD) == 2 \
            else datetime.datetime.strptime(items[i + 1].time.split("-")[0], "%H:%M")
        delta += time2 - time1
        count += delta
        if calc_teachers[items[i].teacher] is None:
            calc_teachers[items[i].teacher] = delta
        else:
            calc_teachers[items[i].teacher] += delta
        if calc_subjects[items[i].subj] is None:
            calc_subjects[items[i].subj] = delta
        else:
            calc_subjects[items[i].subj] += delta

    print_time(calc_teachers, calc_subjects, count)

calc_hours(items)
