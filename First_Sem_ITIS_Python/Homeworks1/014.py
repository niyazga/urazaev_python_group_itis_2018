import requests
import re


def download(s):
    d = requests.get(s, allow_redirects=True)
    arr = s.split("/")
    with open(arr[-1], 'wb') as f:
        f.write(d.content)
    print(arr[-1])


def check(s):
    for match in re.finditer(r'href="[^"]+\.pdf', s):
        s = str(match.group())
        s = s.replace("href=\"", "")
        download(s)



url = 'https://en.wikipedia.org/wiki/London'
#url = 'https://html-templates.info/simple-html-templates/prostoy-sayt-html'

r = requests.get(url, allow_redirects=True)
check(r.text)


