def decorator(func):
    def check_type(*args):
        try:
            for i in range(len(args)):
                if not isinstance(args[i], int):
                    flag = i + 1
                    raise TypeError('This is not good from argument number:', flag)
            func(*args)
        except TypeError:
            raise TypeError('This is not good from argument number:', flag) from None
    return check_type

@decorator
def summa(*args):
    print("Summa = ", sum(args))

summa(1, 2, 3, 4.6, 6)
