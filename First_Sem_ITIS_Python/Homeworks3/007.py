rim_num = input()


def rim_to_arabic(rim_num):
    kw = {"M": 1000, "D": 500, "C": 100, "L": 50, "X": 10, "V": 5, "I": 1}
    arabic_num = 0
    prev = kw.get(rim_num[0])
    count = 1
    for i in range(1, len(rim_num)):
        if rim_num[i] not in kw.keys():
            raise TypeError("This is not roman number")
        num = kw.get(rim_num[i])
        if num > arabic_num + prev != prev:
            raise TypeError('This is not roman number')
        if prev != num:
            count = 1
        if prev < num:
            if prev % 5 == 0 and prev % 10 != 0:
                raise TypeError('This is not roman number')
            elif num % prev > 10:
                raise TypeError('This is not roman number')
            else:
                arabic_num -= prev
                prev = num
                continue
        elif prev == num:
            if prev % 5 == 0 and prev % 10 != 0:
                raise TypeError('This is not roman number')
            count += 1
            if count > 3:
                raise TypeError('This is not roman number')
        arabic_num += prev
        prev = num
    print(arabic_num + prev)


rim_to_arabic(rim_num)
