arr = {"M":1000, "CM":900, "D":500, "CD":400, "C":100, "XC":90, "L":50, "XL":40, "X":10, "IX":9, "V":5, "IV":4, "I":1}

def decorator(func):
    def wrapper(*args):
        try:
            s = func(*args)
            if not isinstance(s, int):
                raise TypeError('This is not good format data')
            ans = ""
            if 4000 > s >= 0:
                for k, v in arr.items():
                    ans += s // v * k
                    s %= v
                return ans
            else:
                raise TypeError('This is not good format data')
        except TypeError:
            raise TypeError('This is not good format data') from None
    return wrapper

@decorator
def summa(*args):
    return sum(args)

@decorator
def kek(a):
    return a


# print(summa(1, 2, 3, 6))
print(kek(12663))