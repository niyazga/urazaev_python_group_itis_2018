import math
import pygame

pygame.init()
pygame.font.init()
myfont = pygame.font.SysFont('', 30)

x = 0
perc = 0
center = 250
r = 100
side = r * 2

screen = pygame.display.set_mode((500, 500))
pygame.display.set_caption("Task_3")
clock = pygame.time.Clock()
running = True
clock.tick(60)

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    screen.fill((255, 255, 255))
    arc = [center - r, center - r, side, side]
    pygame.draw.arc(screen, (0, 0, 255), arc, 0, x, 5)
    if x > 2 * math.pi:
        x = 0
        perc = 0
    x += (2 * math.pi) / 99
    perc += 1

    text = myfont.render(str(perc) + '%', False, (255, 0, 0))
    screen.blit(text, (center - 15, center))
    pygame.display.flip()