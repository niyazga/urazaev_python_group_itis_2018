import pygame as p

p.init()
screen = p.display.set_mode((500, 500))
p.display.set_caption("Task_04")
clock = p.time.Clock()
running = True
x = 0
flag = True
clock.tick(160)

while running:
    for event in p.event.get():
        if event.type == p.QUIT:
            running = False
    screen.fill((255, 255, 255))
    p.draw.circle(screen, (255, 203, 219), [250, 250], 40 + x)

    if flag:
        x += 15
        if x == 150:
            flag = False
    else:
        x -= 2
        if x == 0:
            flag = True
    p.display.flip()