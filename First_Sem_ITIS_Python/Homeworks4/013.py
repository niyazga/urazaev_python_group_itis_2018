class typealias(object):
    def __init__(self, type):
        self.type = type
        self.value = None

    def set(self, value):
        if self.type != type(value):
            raise TypeError('Wrong type')
        else:
            self.value = value

    def get(self):
        return self.value

