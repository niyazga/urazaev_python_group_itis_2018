import requests
import re
from multiprocessing import Pool


def download(links):
    count = 0
    pool = Pool(processes=3)
    results = pool.map(requests.get, links)

    # d = requests.get(s, allow_redirects=True)
    for i in results:
        count += 1
        with open('file'+str(count) + '.pdf', 'wb') as f:
            f.write(i.content)


def check(s):
    links = []
    for match in re.finditer(r'href="[^"]+\.pdf', s):
        s = str(match.group())
        s = s.replace("href=\"", "")
        links.append(s)
    download(links)



url = 'https://en.wikipedia.org/wiki/London'
#url = 'https://html-templates.info/simple-html-templates/prostoy-sayt-html'

r = requests.get(url, allow_redirects=True)
check(r.text)


