import requests
from bs4 import BeautifulSoup


headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'ru',
    'Accept-Encoding': 'br, gzip, deflate',
    'Connection': 'keep-alive',
    'DNT': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
}

key_words = [
    'требования',
    'компетенции',
    'задачи',
]


def get_vacancy_from_url(url):
    response = requests.get(url, headers=headers)
    html_doc = response.text

    soup = BeautifulSoup(html_doc, 'html.parser')

    vacancy = ''
    for title in soup.find_all('title'):
        vacancy += title.get_text() + '\n'

    has_found_any_requirements = False
    for paragraph in soup.find_all('p'):
        paragraph = paragraph.get_text()
        paragraph = paragraph.lower()
        for key_word in key_words:
            if key_word in paragraph:
                vacancy += paragraph + '\n'
                has_found_any_requirements = True

    if not has_found_any_requirements:
        vacancy += 'Требования не были найдены!\n'

    return vacancy

url = 'https://hh.ru/vacancy/29309100'
print(get_vacancy_from_url(url))



