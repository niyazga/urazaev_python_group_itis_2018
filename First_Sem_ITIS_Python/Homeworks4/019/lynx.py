from bs4 import BeautifulSoup

tags_to_skip = [
    '<td>',
    '</td>',
    '</tr>',
    '<li>',
    '</li>',
    '<p>',
    '</p>',
]

BASIC_STATE = 0
FILLING_TABLE_STATE = 1
FILLING_UL_STATE = 2
FILLING_OL_STATE = 3


def table_to_format(table):
    formatted_string = ''

    column_widths = []
    for row in table:
        column_width = 0
        for element in row:
            column_width = max(column_width, len(element))
        column_widths.append(column_width)

    table_width = sum(column_widths) + len(column_widths) * 3 + 1
    formatted_string += ("%s\n" % ('-' * table_width))
    for row in table:
        for i in range(len(row)):
            formatted_string += ("| %s " % row[i].ljust(column_widths[i]))
        formatted_string += ("|\n%s\n" % ('-' * table_width))

    return formatted_string


def html_doc_to_format(html_doc):
    html_doc = BeautifulSoup(html_doc, 'html.parser')
    html_doc_prettified = html_doc.prettify()
    html_lines = html_doc_prettified.split('\n')
    html_lines = [html_line.strip() for html_line in html_lines]

    formatted_string = ''
    state = BASIC_STATE
    table = []
    ordered_list_counter = 0

    for html_line in html_lines:
        if html_line in tags_to_skip:
            continue
        elif html_line == '<table>':
            state = FILLING_TABLE_STATE
        elif html_line == '</table>':
            state = BASIC_STATE
            formatted_string += table_to_format(table)
            table.clear()
        elif html_line == '<tr>':
            table.append([])
        elif html_line == '<ul>':
            state = FILLING_UL_STATE
        elif html_line == '</ul>':
            state = BASIC_STATE
        elif html_line == '<ol>':
            state = FILLING_OL_STATE
        elif html_line == '</ol>':
            state = BASIC_STATE
            ordered_list_counter = 0
        elif html_line == '<hr/>':
            formatted_string += ("%s\n" % ('=' * 50))
        elif html_line == '<br/>':
            formatted_string += '\n'
        else:
            if state == FILLING_TABLE_STATE:
                table[-1].append(html_line)
            elif state == FILLING_UL_STATE:
                formatted_string += ("* %s\n" % html_line)
            elif state == FILLING_OL_STATE:
                ordered_list_counter += 1
                formatted_string += ("%d. %s\n" % (ordered_list_counter, html_line))
            else:
                formatted_string += ("%s\n" % html_line)

    return formatted_string


def get_url(s):
    file = open(s)
    string = ""
    for line in file:
        string+=line
    print(html_doc_to_format(string))


