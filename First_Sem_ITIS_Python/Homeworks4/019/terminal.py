import os, shutil, re
from lynx import get_url

class SuperFile:
    def __init__(self, path):
        path = os.path.abspath(path)
        if not os.path.isfile(path):
            with open(path, 'wb') as f:
                pass
        self.path = path

    def __mul__(self, other):
        name_file = self.path.split("/")[-1].split(".")
        new_file = name_file[0] + "mult%d" % other + "." + name_file[1]
        file = SuperFile(os.path.dirname(self.path) + "/" + new_file)

        with open(self.path) as f:
            lines = f.readlines()
        with open(file.path, 'a') as new_file:
            for i in range(other):
                for line in lines:
                    new_file.write(line)
        return file

    def __add__(self, other):
        first_file = self.path.split("/")[-1].split(".")
        second_file = other.path.split("/")[-1].split(".")
        fileName = first_file[0] + second_file[0] + ".txt"
        new_file = SuperFile(os.path.dirname(self.path) + "/" + fileName)

        with open(new_file.path, 'a') as nf:
            with open(self.path) as f:
                for line in f:
                    nf.write(line)
            nf.write("\n")
            with open(other.path) as f:
                for line in f:
                    nf.write(line)
        return other

    def println(self, s):
        file = open(self.path, 'a')
        file.write(s + "\n")
        file.close()

    def printAll(self):
        print("Файл: %s" % self.path.split("\\")[-1])
        file = open(self.path)
        for line in file:
            print(line, end="")
        file.close()


file1 = SuperFile("kek1.txt")
file2 = SuperFile("kek2.txt")
file1 * 3
file1 + file2


def test():
    path = os.path.abspath("/Users/niyaz/")
    while True:
        path = os.path.abspath(path)
        command = input(path + ": ")

        if not command:
            continue

        s = command.split(" ")
        if s[0] == "cd":
            if s[1][0] == "/":
                new_path = os.path.abspath(s[1])
                if os.path.exists(new_path):
                    path = new_path
                else:
                    print("Упс, не могу найти:(")
            elif s[1]:
                new_path = path + "/" + s[1]
                if os.path.exists(new_path):
                    path = new_path
                else:
                    print("Упс, не могу найти:(")

        elif s[0] == "ls":
            for file in os.listdir(path):
                print(file)

        elif s[0] == "append":
            if s[1].split(".")[-1] != "txt":
                print("Упс, это не текстовый файл:( Пожалуйста, используйте txt файл")
                continue
            f = SuperFile(path + "/" + s[1])
            f.printAll()
            while True:
                some = input()
                if some == "<exit>":
                    flag = input("Вы хотите выйти? Введите Yes или No ")
                    if flag == "Yes":
                        break
                f.println(some)

        elif s[0] == "rm":
            regular = s[1]
            arr = []
            print("Вы точно хотите удалить все эти файлы?")
            for file in os.listdir(path):
                if re.match(regular, file) != None:
                    print(file)
                    arr.append(file)
            print("Введите Yes или No")
            mark = input()
            if mark == "Yes":
                for file in arr:
                    s = file.split(".")
                    if len(s) != 1:
                        os.remove(path + "/" + file)
                    else:
                        shutil.rmtree(path + "/" + file)
            elif mark == "No":
                continue

        elif s[0] == "lynx":
            URL = s[1]
            get_url(URL)

        elif s[0] == "exit":
            return
        else:
            print("Упс, я так не умею((")


test()



