import doctest
import pytest
import unittest

class Vector2D:
    """
    >>> str(a + b)
    '<2, 2>'
    >>> str(a - b)
    '<2, -2>'
    >>> abs(a)
    2.0
    >>> abs(b)
    2.0
    >>> a == b
    False
    >>> a != b
    True
    >>> str(a * b)
    '<0, 0>'
    """

    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def __str__(self):
        return "<%s, %s>" % (self.x, self.y)

    def __add__(self, v):
        return Vector2D(self.x + v.x, self.y + v.y)

    def __call__(self, *args, **kwargs):
        print("crly ? u r callin' vec?")

    def __mul__(self, k):
        if type(k) == int:
            return Vector2D(self.x * k + self.y * k)
        else:
            return Vector2D(self.x * k.x, self.y * k.y)

    def __sub__(self, k):
        return Vector2D(self.x - k.x, self.y - k.y)

    def __abs__(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5

    def __eq__(self, other):
        return True if self.x == other.x and self.y == other.y else False


@pytest.fixture
def a():
    return Vector2D(2, 0)


@pytest.fixture
def b():
    return Vector2D(0, 2)


def test_add(a, b):
    assert str(a + b) == '<2, 2>'


def test_sub(a, b):
    assert str(a - b) == '<2, -2>'


def test_abs(a, b):
    assert abs(a) == 2.0
    assert abs(b) == 2.0


def test_eq(a, b):
    assert (a == b) is False
    assert (a != b) is True


def test_mul(a, b):
    assert str(a * b) == '<0, 0>'


class TestVector2D(unittest.TestCase):
    def setUp(self):
        self.a = Vector2D(2, 0)
        self.b = Vector2D(0, 2)

    def tearDown(self):
        pass

    def test_add(self):
        self.assertEqual(str(self.a + self.b), '<2, 2>')

    def test_sub(self):
        self.assertEqual(str(self.a - self.b), '<2, -2>')

    def test_abs(self):
        self.assertEqual(abs(self.a), 2.0)
        self.assertEqual(abs(self.b), 2.0)

    def test_eq(self):
        self.assertFalse(self.a == self.b)
        self.assertTrue(self.a != self.b)

    def test_mul(self):
        self.assertEqual(str(self.a * self.b), '<0, 0>')


v1 = Vector2D(1, 2)
v2 = Vector2D(1, 2)
v3 = v1 + v2
print(v3)

print(v2 - v1)
print(v3)
print(abs(v1))
print(v1 == v2)

if __name__ == '__main__':
    doctest.testmod(extraglobs={'a': Vector2D(2, 0), 'b': Vector2D(0, 2)})
    unittest.main()
