from django.conf.urls import url
from . import views
from django.urls import path
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('main', views.tweets_main.as_view()),
    path('login/', auth_views.LoginView.as_view())
]
