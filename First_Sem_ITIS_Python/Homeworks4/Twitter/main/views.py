from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils import timezone
from django.shortcuts import redirect
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login

from .models import Tweet


# Create your views here.
class tweets_main(LoginRequiredMixin, View):
    def get(self, request):
        twits = reversed(Tweet.objects.order_by("date"))
        return render(request, 'index.html', {'twits': twits})

    def post(self, request):
        twits = reversed(Tweet.objects.order_by("date"))
        if request.POST.get('title') != None:
            post = Tweet(
                title=request.POST.get('title'),
                text=request.POST.get('text'),
                date=timezone.now()
            )
            post.save()

        return render(request, 'index.html', {'twits': twits})

# def login_view(request):
#     render(request, 'login.html')
#     username = request.POST['username']
#     password = request.POST['password']
#     user = authenticate(username = username, password = password)
#     if user is not None:
#         if user.is_active:
#             login(request, user)
#             user.save()
#             return index(request)
#     #     pass
#         else:
#             print('kek')
#     else:
#         print('kek2')
