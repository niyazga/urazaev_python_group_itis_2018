from django.conf.urls import url
from django.urls import path

from . import views


urlpatterns = [
    url(r'^$', views.twits_all, name='main'),
    url(r'^main', views.twits_all, name='main'),
    url(r'^login', views.login, name='login')
]