from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.contrib import auth

from .models import Twit
from django.utils import timezone


# Create your views here.

def twits_all(request):

    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login')

    if request.POST.get('text') is not None:
        post = Twit(
            title=request.user.username,
            text=request.POST.get('text'),
            date=timezone.now()
        )
        post.save()
    twits = reversed(Twit.objects.order_by('date'))
    return render(request, 'index.html', {'twits': twits})


def login(request):
    twits = reversed(Twit.objects.order_by("date"))
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            return HttpResponseRedirect('/main')
        else:
            print('Something happened bad')
            return render(request, 'registration/login.html', {'twits': twits})
    return render(request, 'registration/login.html', {'twits': twits})
