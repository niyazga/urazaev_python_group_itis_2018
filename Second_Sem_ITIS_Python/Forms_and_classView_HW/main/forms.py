from django.forms import ModelForm
from .models import Colors


class colorForm(ModelForm):
    class Meta:
        model = Colors
        fields = '__all__'
