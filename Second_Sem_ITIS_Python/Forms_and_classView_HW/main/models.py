from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models


# Create your models here.
def validate_age(value):
    if value < 18:
        raise ValidationError('You must be over than 18')


class Colors(models.Model):
    colors_choices = (
        ('r', 'red'),
        ('g', 'green'),
        ('b', 'blue'),
    )
    name = models.CharField(max_length=123, validators=[RegexValidator(regex='^[a-zA-Z]+$', message='Only a-zA-z characters')])
    age = models.IntegerField(validators=[validate_age])
    color = models.CharField(max_length=123, choices=colors_choices)
