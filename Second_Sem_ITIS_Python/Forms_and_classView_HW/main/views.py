from django.shortcuts import render
from django.views.generic import ListView

from main.forms import colorForm
from django.views.generic.edit import FormView

# Create your views here.
from main.models import Colors


# class ColorView(FormView):
#     template_name = 'main/main.html'
#     form_class = colorForm
#     success_url = '/'
#
#
#
#     def form_valid(self, form):
#         form.save()
#         return super(ColorView, self).form_valid(form)


class MyListView(ListView):
    template_name = "main/colors.html"
    model = Colors

    def get_context_data(self, **kwargs):
        context = super(MyListView, self).get_context_data(**kwargs)
        all = Colors.objects.all()
        color = []
        name = []
        for i in all:
            color.append(i.color)
            name.append(i.name)
        context["colors"] = color
        context["name"] = name
        return context
