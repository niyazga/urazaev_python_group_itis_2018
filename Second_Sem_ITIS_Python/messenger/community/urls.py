from django.urls import path

from community import views

urlpatterns = [
    path('<int:id>', views.page_detail, name='group')
]