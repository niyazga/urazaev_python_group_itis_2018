from django.db import models


# Create your models here.
class Document(models.Model):
    file = models.FileField(null=True, blank=True)
