from django.apps import AppConfig


class ForAdminConfig(AppConfig):
    name = 'for_admin'
