from django.urls import path

from for_admin import views

urlpatterns = [
    path('', views.all_peoples, name='all_peoples')
]