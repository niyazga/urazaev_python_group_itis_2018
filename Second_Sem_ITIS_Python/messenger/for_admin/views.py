from django.shortcuts import render

# Create your views here.
from profile_page.models import ProfilePage
from django.contrib.auth.decorators import permission_required


@permission_required('auth.permission.can_change_permission')
def all_peoples(request):
    peoples = ProfilePage.objects.all()
    return render(request, 'for_admin/admin.html', {'peoples':peoples})