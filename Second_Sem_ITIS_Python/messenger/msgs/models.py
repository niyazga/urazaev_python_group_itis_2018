# from django.contrib.auth.models import User
from django.db import models
from django.utils.timezone import now
from messenger.settings import CUT_MSG_TEXT_LENGTH


# Create your models here.


class Msg(models.Model):
    user_send = models.ForeignKey('auth.User', related_name="send", on_delete=models.PROTECT)
    user_src = models.ForeignKey('auth.User', related_name="src", on_delete=models.PROTECT)
    text = models.TextField()
    date = models.DateTimeField(default=now)
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return "От:%s, Кому:%s, Текст:%s" % (self.user_send.first_name, self.user_src.first_name, self.text)

    def cut(self):
        return self.text[:CUT_MSG_TEXT_LENGTH] + "..."



