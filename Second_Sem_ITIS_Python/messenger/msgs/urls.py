from django.contrib import admin
from django.urls import path, include
from django.views.generic import ListView
from django.views.generic.detail import DetailView

from msgs import views
from msgs.models import Msg

urlpatterns = [
    path('im/', views.MyListView.as_view(), name='im'),
    path('im/<int:id>', views.message_detail, name='detail'),
]
