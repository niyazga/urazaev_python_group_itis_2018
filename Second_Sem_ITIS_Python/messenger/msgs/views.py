from django.contrib.auth.decorators import login_required
from django.db.models import F
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic.list import ListView

from msgs.models import Msg


# Create your views here.
class MyListView(ListView):
    template_name = "msgs/all_messages.html"
    model = Msg

    def get_context_data(self, **kwargs):
        context = super(MyListView, self).get_context_data(**kwargs)
        context["msgs"] = Msg.objects.filter(user_src=self.request.user)\
            .exclude(user_send__first_name__contains=F('user_send__last_name')).order_by("-date")\
            .values('user_send__first_name', 'user_send__last_name', 'is_read', 'text', 'date', 'pk')
        return context


def message_detail(request, id):
    msg = Msg.objects.get(pk=id)
    if not msg.is_read:
        msg.is_read = True
        msg.save()
    if request.POST:
        new_msg = Msg(
            user_send=request.user,
            user_src=msg.user_send,
            text=request.POST['text']
        )
        new_msg.save()
    return render(request, "msgs/messages_detail.html", {"msg": msg})
