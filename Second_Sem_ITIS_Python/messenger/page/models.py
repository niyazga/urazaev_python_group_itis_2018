from django.db import models
from documents.models import Document
from music.models import Music
from photos.models import Album, Picture
from videos.models import Video
from walls.models import Wall, Article


class Location(models.Model):
    country = models.TextField()
    city = models.TextField()


class Page(models.Model):
    class Meta:
        abstract = True

    creation_date = models.DateField()
    url_address = models.URLField()
    status = models.CharField(max_length=100)
    website = models.URLField(null=True)
    walls = models.ForeignKey(
        Wall,
        related_name="%(app_label)s_%(class)s_related",
        on_delete=models.PROTECT,
        default=None,
        null=True,
        blank=True
    )
    musics = models.ManyToManyField(Music, related_name="%(app_label)s_%(class)s_related")
    videos = models.ManyToManyField(Video, related_name="%(app_label)s_%(class)s_related")
    albums = models.ManyToManyField(Album, related_name="%(app_label)s_%(class)s_related")
    articles = models.ManyToManyField(Article, related_name="%(app_label)s_%(class)s_related",
                                      null=True, blank=True)
    page_picture = models.ManyToManyField(Picture, related_name="%(app_label)s_%(class)s_related")
    location = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        null=True,
        related_name="%(app_label)s_%(class)s_related",
        blank=True
    )
    subscribes = models.ManyToManyField('profile_page.ProfilePage', related_name="%(app_label)s_%(class)s_related",
                                        null=True, blank=True)


class VerificationMixin(models.Model):
    verification = models.NullBooleanField(default=False)


class DocumentsMixin(models.Model):
    documents = models.ManyToManyField(Document, default=None)
