from django import forms
from django.contrib.auth.models import User
from django.core import validators
from walls.models import Article


class LoginForm(forms.ModelForm):
    password = forms.CharField(min_length=8, max_length=20, widget=forms.PasswordInput)
    email = forms.CharField(validators=[validators.validate_email])

    class Meta:
        model = User
        fields = ["email", "password"]


class SignUpForm(forms.ModelForm):
    password = forms.CharField(min_length=8, max_length=20, widget=forms.PasswordInput)
    email = forms.CharField(validators=[validators.validate_email])

    class Meta:
        model = User
        fields = ["email", "password", "username"]


class PostForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ["text", "image"]