from django.contrib.staticfiles.urls import static
from django.urls import path
from django.views.generic import FormView

from messenger.settings import MEDIA_URL, MEDIA_ROOT
from profile_page import views
from profile_page.forms import SignUpForm

urlpatterns = [
    path('login/', views.login, name='login'),
    # path('signup/', views.MyFormView.as_view, name='signup'),
    path('people/<int:id>', views.page_detail, name='profile_page'),
    path('like/<int:id>', views.like, name = 'like')
]

urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)
