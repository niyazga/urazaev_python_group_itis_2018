from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse
from profile_page.forms import LoginForm, PostForm
from profile_page.models import ProfilePage
from walls.models import Article


@login_required
def page_detail(request, id):
    name = request.session['param']
    page = ProfilePage.objects.get(pk=id)
    avatar = page.page_picture.order_by("-date")[0]
    q1 = Q(subscribes=page)
    q2 = Q(subscribe_to=page)
    q3 = q1 & q2
    q4 = q1 & ~q2
    friends = ProfilePage.objects.filter(q3)
    subs = ProfilePage.objects.filter(q4)
    wall = page.walls.articles.all()
    groups = page.subscribe_to_groups.all()
    form = PostForm()
    if request.POST:
        f = PostForm(request.POST, request.FILES)
        q = f.save()
        q.save()

    return render(request, 'profile_page/profile_page.html',
                  {
                      'site': name,
                      'user': request.user,
                      'page': page,
                      'avatar': avatar,
                      'subs': subs,
                      'friends': friends,
                      'wall': wall,
                      'groups': groups,
                      'form': form
                  })


def login(request):
    request.session['param'] = 'VK'
    if request.user.is_authenticated:
        return redirect(reverse("profile_page", args=(request.user.id,)))
    form = LoginForm()
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            username = ProfilePage.objects.get(user__email=email).user.username
            user = authenticate(username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return redirect(reverse("profile_page", args=(request.user.id,)))
    return render(request, 'profile_page/login.html', {"form": form})


def like(request, id):
    article = Article.objects.get(pk=id)
    count = article.like + 1
    article.like = count
    article.save()
    return HttpResponse(count)


