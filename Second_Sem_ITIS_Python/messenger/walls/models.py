import os

from django.db import models
import datetime


# from profile_page.models import ProfilePage


class Article(models.Model):

    def path(instance, filename):
        return os.path.join(instance.who_posted.user.username, filename)

    text = models.TextField()
    date = models.DateTimeField(default=datetime.datetime.now())
    image = models.ImageField(upload_to=path, null=True, blank=True)
    like = models.IntegerField(default=0)

    who_posted = models.ForeignKey(
        'profile_page.ProfilePage',
        on_delete=models.PROTECT,
        default=1,
        related_name="who_posted"
    )


class Wall(models.Model):
    articles = models.ManyToManyField(Article)
